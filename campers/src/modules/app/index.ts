import {ContainerComponent} from './components/container/component';
import {TopbarComponent} from './components/topbar/';
import Reservations from './components/reservations/';

export default angular.module('app.application', [Reservations])
    .directive('campersApp', () => new ContainerComponent())
    .directive('topbar', () => new TopbarComponent())
    .config(($urlRouterProvider: angular.ui.IUrlRouterProvider) => {
        $urlRouterProvider.otherwise('/home');
    }).name;