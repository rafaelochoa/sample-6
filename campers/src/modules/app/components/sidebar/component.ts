import {Controller} from './controller';
export class SidebarComponent {
    public controllerAs: string = 'vm';
    public template: string = require('./index.html');
    public controller: Function = Controller;
}