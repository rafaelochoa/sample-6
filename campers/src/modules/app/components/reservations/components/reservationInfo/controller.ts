import {IReservationService} from '../../core/services/IReservationService';
export interface IReservationInfoController {
    activateReservation(reservation): void ;
    showEditReservation(reservation): void;
}

export class Controller implements IReservationInfoController {
    public static $inject: Array<string> = ['IReservationService', '$uibModal'];
    
    constructor(private reservationService: IReservationService, private $modal: ng.ui.bootstrap.IModalService) {
        
    }
    
    activateReservation(reservation): void {
        reservation.available = false;
        this.reservationService.updateReservation(reservation, false);
    }
    
    showEditReservation(reservation): void {
        const options: ng.ui.bootstrap.IModalSettings = {
            template: '<reservation-edit></reservation-edit>',
            bindToController: true
        };
        this.reservationService.setCurrentReservation(angular.copy(reservation));
        this.$modal.open(options);
    }
    
}