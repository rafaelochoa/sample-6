import {Controller} from './controller';
export class ReservationMainComponent implements ng.IDirective {
    public restrict: string = 'E';
    public controllerAs: string = 'mainVm';
    public template: string = require('./index.html');
    public controller: Function = Controller;
}
