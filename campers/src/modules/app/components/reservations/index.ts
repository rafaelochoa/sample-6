import config from './core/config';
import {PhoneFilter} from './core/filters/PhoneFilter';
import {PageReservationComponent} from './components/pageReservations/component';
import {ReservationInfoComponent} from './components/reservationInfo/component';
import {ReservationEditComponent} from './components/reservationEdit/component';
import {ReservationMainComponent} from './components/reservationMain/component';
import {SidebarComponent} from '../sidebar/component';
import {SidebarService} from '../sidebar/service';
import {ReservationService} from './core/services/ReservationService';
import {DataService} from './core/services/DataService';

export default angular.module('app.reservations', [])
    .directive('sidebar', () => new SidebarComponent())
    .directive('pageReservations', () => new PageReservationComponent())
    .directive('reservationMain', () => new ReservationMainComponent())
    .directive('reservationInfo', () => new ReservationInfoComponent())
    .directive('reservationEdit', () => new ReservationEditComponent())
    .service('IReservationService', ReservationService)
    .service('IDataService', DataService)
    .service('ISidebarService', SidebarService)
    .filter('phone', PhoneFilter)
    .config(config).name;
