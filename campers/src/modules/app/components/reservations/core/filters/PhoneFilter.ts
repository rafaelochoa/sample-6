export function PhoneFilter() {
    
    return (phone) => {
        let country;
        let city;
        let number;
        
        if (!phone) {
            return '';
        }
        const value = phone.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return phone;
        }
        switch (value.length) {
            case 10:
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;
            case 11:
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;
            case 12:
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;
            default:
                return phone;
        }
        
        if (country == 1) {
            country = '';
        }
        return (`${country} (${city}) ${number.slice(0, 3)}-${number.slice(3)}`).trim();
    }
    
}
