function config($stateProvider: angular.ui.IStateProvider, localStorageServiceProvider: any): void {
    $stateProvider.state('home', {
        url: '/home',
        template: '<page-reservations></page-reservations>'
    });
    localStorageServiceProvider
        .setPrefix('campersApp')
        .setNotify(true, true);
}
config.$inject = ['$stateProvider', 'localStorageServiceProvider'];
export default config;