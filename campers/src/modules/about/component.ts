export default angular.module('app.about', [])
    .component('pageAbout', {template: require('./index.html')})
    .config(($stateProvider: angular.ui.IStateProvider) => {
        $stateProvider.state('about', {
            url: '/about',
            template: '<page-about></page-about>'
        });
    }).name;