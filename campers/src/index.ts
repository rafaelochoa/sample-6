/// <reference path="../typings/main.d.ts" />
import 'angular';
import 'angular-animate';
import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'ng-fx';
import 'angular-local-storage/dist/angular-local-storage';
import 'angular-ui-mask';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import './styles/bootswatch.paper.min.css';
import './styles/screen.scss';
import App from './modules/app/';
import About from './modules/about/component';

angular.module('app', ['ui.router', 'ngAnimate', 'ui.bootstrap', 'ng-fx', 'LocalStorageModule', 'ui.mask', App, About]);
angular.bootstrap(document, ['app'], {strictDi: true});
