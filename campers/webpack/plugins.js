var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
    new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body',
            hash: true
        }
    ),
    new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.jquery': 'jquery',
            _: 'lodash'
        }
    ),
    new webpack.DefinePlugin({
        'process.env.npm_config_server': JSON.stringify(process.env.npm_config_server || 'dev')
    })
];


